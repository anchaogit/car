// 通用标准函数

var ajax_get_func=function(url,token,param,handle_func=""){
    url=url+"/?what="+param
    // console.log("get",url)
    var token_info = ""
    var phone = ""
    if(token != ""){
        token = JSON.parse(token)
        token_info = token.token
        phone = token.phone
    }
    $.ajax({
        url: url,
        headers:{"token":token_info,"phone":phone},
        type: 'get',
        dataType: 'JSON',
        cache: false,
        success: function (v) {

            if(v.code==0){
                var data=v.data

                if (handle_func!==""){
                    handle_func(data)

                }
            }
            if(v.msg!==""){
                layer.msg(v.msg)
            }
        },
        error:function(d){
            console.log(d)
        }
    });
}

var ajax_post_func=function(url,token,param,data,handle_func=""){
    url=url+"/?what="+param
    // console.log("post",url)
    var token_info = ""
    var phone = ""
    if(token != ""){
        token = JSON.parse(token)
        token_info = token.token
        phone = token.phone
    }
    $.ajax({
        url: url,
        headers:{"token":token_info,"phone":phone},
        type: 'post',
        contentType: 'application/json',
        data:data,
        success: function (v) {
            // console.log(v)
            if(v.code==0){
                var data=v.data
                if (handle_func!==""){
                    handle_func(data)
                }

            }
            if(v.msg!==""){
                layer.msg(v.msg)
            }
            
        },
        error:function(d){
            console.log(d)
        }
    });
}

var copy_func = function(d){
    var v = JSON.stringify(d)
    v = JSON.parse(v)
    return v
}


// 数组中获取服务查询条件的单个
var query_detail = function(d,f,k){
    var res;
    d.forEach(item=>{
        if(item[f]==k){
            res=item
        }
    })
    return res
}


// 搜索功能
var search_func=function(tagret_data,search_key,search_kind,...target_field){
  // console.log(tagret_data,search_key,target_field1,target_field2)
    if(tagret_data == "" || tagret_data == null || search_key == "" || search_key == null ||target_field == "" || target_field == null ){
        return tagret_data
    }

    var d = JSON.stringify(tagret_data)
    d = JSON.parse(d) 
    target_field = target_field[0]
    result_data=[]
    for(var i=0;i<d.length;i++){
        for(var n = 0;n < target_field.length;n++){
            if (d[i][target_field[n]] == null){
                continue
            }
            
            if(search_kind != null){
                // 精准查询
                if (d[i][target_field[n]] == search_key){
                    result_data.push(d[i])
                    break
                }

            }else{
                // 模糊查询

                if (d[i][target_field[n]].indexOf(search_key) != -1){
                    result_data.push(d[i])
                    break
                }
            }
            
        }
    }
    return result_data
}


var get_detail_from_key=function(data,key,field){
    if(key == null || field == null || data == null){
        return
    }
  for(var i=0;i<data.length;i++){
    if(data[i][field]==key){
      return data[i]
    }
  }
}

// 图片上传功能
var img_upload_func=function(btn,show,func){
  upload.render({
  elem: '#'+btn
  ,auto: false
  ,accept:"file"
  ,choose: function(obj){
    obj.preview(function(index, file, result){
      // result是图片 base64编码
      if (result.length>8000000){
        layer.msg("图片不能超过5MB")
        return false
      }else{
        $('#'+show).attr('src',result)
        if(func != null){
            func(file.name,result)
        }
      }
      
    });
  }
});
}



// 填充信息
var fill_option=function(fill_data,list_id,key,title=''){
  $('#'+list_id).empty()
  if(title==''){
    for (var i = fill_data.length - 1; i >= 0; i--) {
      var option='<option value="'+fill_data[i][key]+'">'+fill_data[i][key]+'</option>'
      $('#'+list_id).append(option)
    }
  }else{
    for (var i = fill_data.length - 1; i >= 0; i--) {
        // console.log(fill_data[i][key])
      var option='<option value="'+fill_data[i][key]+'">'+fill_data[i][title]+'</option>'
      $('#'+list_id).append(option)
    }
  }
}

//封装数据
var package_data_base=function(data){
  data.id = parseInt(data.id)
  if(isNaN(data.id)){
    data.id = 0
  }
  return data
}

// 时间戳转时间字符
var formatDate = function (v) {
    var date = new Date(v);
    if (v == "now"){
        date = new Date();
    }
    var YY = date.getFullYear() + '-';
    var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return YY + MM + DD +" "+hh + mm + ss;
}


var formatDate1 = function (v) {
    var date = new Date(v);
    if (v == "now"){
        date = new Date();
    }
    var YY = date.getFullYear();
    var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return YY + MM + DD + hh + mm + ss;
}


// #wgs84坐标系转换成百度坐标系计算函数
function wgs84_bdmap(input_lat,input_lng){

    var pi = 3.1415926535897932384626;
    var a = 6378245.0;
    var ee = 0.00669342162296594323;
    var x_pi = 3.14159265358979324 * 3000.0 / 180.0;


    function wgs2bd(lat, lon){ 
           _wgs2gcj = wgs2gcj(lat, lon);
           _gcj2bd = gcj2bd(_wgs2gcj[0], _wgs2gcj[1]);
           return _gcj2bd;
       }
    

    function gcj2bd(lat, lon){
           x = lon
           y = lat
           z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
           theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);
           bd_lon = z * Math.cos(theta) + 0.0065;
           bd_lat = z * Math.sin(theta) + 0.006;
           return [ bd_lat, bd_lon ];
       }
    

    function bd2gcj(lat, lon){ 
           x = lon - 0.0065
           y = lat - 0.006
           z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
           theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
           gg_lon = z * Math.cos(theta);
           gg_lat = z * Math.sin(theta);
           return [ gg_lat, gg_lon ];
       }
    

    function wgs2gcj(lat, lon){ 
           dLat = transformLat(lon - 105.0, lat - 35.0);
           dLon = transformLon(lon - 105.0, lat - 35.0);
           radLat = lat / 180.0 * pi;
           magic = Math.sin(radLat);
           magic = 1 - ee * magic * magic;
           sqrtMagic = Math.sqrt(magic);
           dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
           dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);
           mgLat = lat + dLat;
           mgLon = lon + dLon;
           return [ mgLat, mgLon ]
       }
    

    function transformLat(lat, lon){ 
           ret = -100.0 + 2.0 * lat + 3.0 * lon + 0.2 * lon * lon + 0.1 * lat * lon + 0.2 * Math.sqrt(Math.abs(lat));
           ret += (20.0 * Math.sin(6.0 * lat * pi) + 20.0 * Math.sin(2.0 * lat * pi)) * 2.0 / 3.0;
           ret += (20.0 * Math.sin(lon * pi) + 40.0 * Math.sin(lon / 3.0 * pi)) * 2.0 / 3.0;
           ret += (160.0 * Math.sin(lon / 12.0 * pi) + 320 * Math.sin(lon * pi  / 30.0)) * 2.0 / 3.0;
           return ret;
       }
    

    function transformLon(lat, lon){ 
           ret = 300.0 + lat + 2.0 * lon + 0.1 * lat * lat + 0.1 * lat * lon + 0.1 * Math.sqrt(Math.abs(lat));
           ret += (20.0 * Math.sin(6.0 * lat * pi) + 20.0 * Math.sin(2.0 * lat * pi)) * 2.0 / 3.0;
           ret += (20.0 * Math.sin(lat * pi) + 40.0 * Math.sin(lat / 3.0 * pi)) * 2.0 / 3.0;
           ret += (150.0 * Math.sin(lat / 12.0 * pi) + 300.0 * Math.sin(lat / 30.0 * pi)) * 2.0 / 3.0;
           return ret;
        }

    wgs2bd=wgs2bd(input_lat,input_lng)
    wgs2bd.reverse()
    return wgs2bd

}
